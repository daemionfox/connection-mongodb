<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 7/2/16
 * Time: 10:27 AM
 */

namespace RCSI\Connection;


use MongoClient;
use rcsi\Exceptions\MongoHostException;
use RCSI\Wrapper\ConfigWrapper;
use RCSI\Wrapper\MonologWrapper;

class MongoDBConnection
{


    /** Static instance of the MongoClient
     * @var MongoClient
     */
    public static $mongo;

    /** Static instance of the MongoConnection
     * @var
     */
    public static $connection;

    /** Name of the Connection
     * @var string
     */
    public $collection;

    /** Name of the Database
     * @var string
     */
    public $database;

    protected static $failedHosts = array();

    /**
     * Client Builder
     * @param string $db
     * @param string $user
     * @param string $pass
     * @param string $location
     * @param int $port
     * @return MongoClient
     */
    public static function client($db, $user, $pass, $location = 'localhost', $port = 27017)
    {
        $serverString = "mongodb://";
        if($user != null && $pass !== null) {
            $serverString .= "{$user}:{$pass}@";
        }
        $serverString .= $location;
        if ($port !== null) {
            $serverString .= ":{$port}";
        }
        if ($db !== null) {
            $serverString .= "/{$db}";
        }
        return new MongoClient($serverString);

    }

    /** Initialize MongoClient
     * @param null $location
     * @return MongoClient
     * @throws MongoHostException
     */
    public static function init($location = null)
    {
        if (self::$connection === null) {
            $config = ConfigWrapper::init();
            $logger = MonologWrapper::init();
            $mongoHostArray = $config->getArray('mongoHost');
            $mongoLocation = $location !== null ? $location : array_shift($mongoHostArray);
            try {
                if (self::$mongo === null && $mongoLocation !== null) {
                    $mongoUser     = $config->get('mongoUser');
                    $mongoPass     = $config->get('mongoPass');
                    $mongoPort     = $config->get('mongoPort');
                    $mongoDB       = $config->get('mongoDB');
                    $logger->addDebug("Attempting to connect to MongoDB");
                    self::$mongo = self::client($mongoDB, $mongoUser, $mongoPass, $mongoLocation, $mongoPort);
                }
            } catch (\MongoConnectionException $mce) {
                $logger->addError("Could not connect to: $mongoLocation");
                self::$failedHosts[] = $mongoLocation;
                $newlocation = array_shift($mongoHostArray);
                while (in_array($newlocation, self::$failedHosts)) {
                    if (empty($mongoHostArray)) {
                        throw new MongoHostException("No available hosts to connect to: {$mce->getMessage()}");
                    }
                    $newlocation = array_shift($mongoHostArray);
                }
                return self::init($newlocation);
            } catch (\MongoException $me) {
                $logger->addError("Could not connect to: $mongoLocation");
                self::$failedHosts[] = $mongoLocation;
                $newlocation = array_shift($mongoHostArray);
                while (in_array($newlocation, self::$failedHosts)) {
                    if (empty($mongoHostArray)) {
                        throw new MongoHostException("No available hosts to connect to: {$me->getMessage()}");
                    }
                    $newlocation = array_shift($mongoHostArray);
                    return self::init($newlocation);
                }
            }
        }
        return self::$mongo;
    }

    /**
     * Disconnect the database connection
     */
    public static function disconnect()
    {
        return self::$mongo->close();
    }

    public static function destroy()
    {
        self::$mongo = null;
    }

    /** Set the MongoClient (used for testing)
     * @param MongoClient $client
     */
    public static function setMongoClient(MongoClient $client)
    {
        self::$mongo = $client;
    }

}
