<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 7/2/16
 * Time: 10:26 AM
 */

namespace RCSI\Enumerations;

use Eloquent\Enumeration\AbstractEnumeration;

class MongoQueryEnum extends AbstractEnumeration
{
    const MONGODB_REGEX  = '$regex';
    const MONGODB_IN     = '$in';
    const MONGODB_GT     = '$gt';
    const MONGODB_LT     = '$lt';
    const MONGODB_LOOKUP = '$lookup';

    public static function isReservedKey($key)
    {
        $local = array(
            self::MONGODB_REGEX,
            self::MONGODB_IN,
            self::MONGODB_LT,
            self::MONGODB_GT,
            self::MONGODB_LOOKUP,
        );

        foreach ($local as $l) {
            if ($key === $l) {
                return true;
            }
        }

        return false;
    }
}
