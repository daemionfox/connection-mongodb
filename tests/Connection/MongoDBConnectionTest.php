<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 7/2/16
 * Time: 10:29 AM
 */

namespace tests\Connection;

use org\bovigo\vfs\vfsStream;
use PHPUnit_Framework_TestCase;
use RCSI\Connection\MongoDBConnection;
use RCSI\Config;
use RCSI\Wrapper\ConfigWrapper;

class MongoDBConnectionTest extends PHPUnit_Framework_TestCase
{
    public $mongoClient;
    public $vfs;

    public function setup()
    {
        $this->mongoClient = $this->getMockBuilder('MongoClient')
            ->disableOriginalConstructor()
            ->getMock();

        $this->mongoClient->expects($this->any())
            ->method('close')
            ->willReturn(true);

        $prodStruct = array(
            "config" => array(
                "config.ini" => '[MONGOPROD]
mongoHost=localhost
mongoUser=nouser
mongoPass=nopass
mongoPort=27017
mongoDB=admin
')
        );
        $this->vfs = vfsStream::setup('root');
        vfsStream::create($prodStruct);
        ConfigWrapper::setPath(vfsStream::url('root/config'));

    }

    public function testInit()
    {
        MongoDBConnection::setMongoClient($this->mongoClient);
        $mongo = MongoDBConnection::init();
        $this->assertInstanceOf('\MongoClient', $mongo);
    }

    public function testDisconnect()
    {
        MongoDBConnection::setMongoClient($this->mongoClient);
        $mongo = MongoDBConnection::init();
        $this->assertInstanceOf('\MongoClient', $mongo);
        $status = MongoDBConnection::disconnect();
        $this->assertTrue($status);
    }


}
